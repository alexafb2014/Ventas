SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `sistemaweb` DEFAULT CHARACTER SET utf8 ;
USE `sistemaweb` ;

-- -----------------------------------------------------
-- Table `sistemaweb`.`cliente`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemaweb`.`cliente` (
  `idcliente` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(30) NULL DEFAULT NULL ,
  `apellidos` VARCHAR(30) NULL DEFAULT NULL ,
  `dni` CHAR(8) NULL DEFAULT NULL ,
  `direccion` VARCHAR(45) NULL DEFAULT NULL ,
  `telefono` VARCHAR(15) NULL DEFAULT NULL ,
  `ruc` CHAR(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`idcliente`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaweb`.`empleado`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemaweb`.`empleado` (
  `idempleado` INT(11) NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(45) NULL DEFAULT NULL ,
  `apellido` VARCHAR(45) NULL DEFAULT NULL ,
  `dni` CHAR(8) NULL DEFAULT NULL ,
  `direccion` VARCHAR(45) NULL DEFAULT NULL ,
  `telefono` VARCHAR(45) NULL DEFAULT NULL ,
  `estado` BINARY(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`idempleado`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaweb`.`igv`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemaweb`.`igv` (
  `idIGV` INT(11) NOT NULL AUTO_INCREMENT ,
  `porcentaje` DOUBLE NULL DEFAULT NULL ,
  PRIMARY KEY (`idIGV`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaweb`.`datos_empresa`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemaweb`.`datos_empresa` (
  `iddatos_empresa` INT(11) NOT NULL AUTO_INCREMENT ,
  `razon_social` VARCHAR(45) NULL DEFAULT NULL ,
  `ruc` CHAR(11) NULL DEFAULT NULL ,
  `telefono` VARCHAR(15) NULL DEFAULT NULL ,
  `direccion` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`iddatos_empresa`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaweb`.`tipo_comprobante`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemaweb`.`tipo_comprobante` (
  `idtipo_comprobante` INT(11) NOT NULL ,
  `descripcion` VARCHAR(45) NULL DEFAULT NULL ,
  PRIMARY KEY (`idtipo_comprobante`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaweb`.`comprobante`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemaweb`.`comprobante` (
  `idcomprobante` INT(11) NOT NULL AUTO_INCREMENT ,
  `fecha` DATETIME NULL DEFAULT NULL ,
  `totalsinIGV` DOUBLE NULL DEFAULT NULL ,
  `totalconIGV` DOUBLE NULL DEFAULT NULL ,
  `idempleado` INT(11) NULL DEFAULT NULL ,
  `idcliente` INT(11) NULL DEFAULT NULL ,
  `idIGV` INT(11) NULL DEFAULT NULL ,
  `iddatos_empresa` INT(11) NULL DEFAULT NULL ,
  `idtipo_comprobante` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`idcomprobante`) ,
  INDEX `comprobante-empleado` (`idempleado` ASC) ,
  INDEX `comprobante_cliente` (`idcliente` ASC) ,
  INDEX `Comprobante_IGV` (`idIGV` ASC) ,
  INDEX `comprobante_empresa` (`iddatos_empresa` ASC) ,
  INDEX `comprobante_tipocomprobante` (`idtipo_comprobante` ASC) ,
  CONSTRAINT `comprobante-empleado`
    FOREIGN KEY (`idempleado` )
    REFERENCES `sistemaweb`.`empleado` (`idempleado` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `comprobante_cliente`
    FOREIGN KEY (`idcliente` )
    REFERENCES `sistemaweb`.`cliente` (`idcliente` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `Comprobante_IGV`
    FOREIGN KEY (`idIGV` )
    REFERENCES `sistemaweb`.`igv` (`idIGV` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `comprobante_empresa`
    FOREIGN KEY (`iddatos_empresa` )
    REFERENCES `sistemaweb`.`datos_empresa` (`iddatos_empresa` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `comprobante_tipocomprobante`
    FOREIGN KEY (`idtipo_comprobante` )
    REFERENCES `sistemaweb`.`tipo_comprobante` (`idtipo_comprobante` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaweb`.`proveedor`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemaweb`.`proveedor` (
  `idproveedor` INT(11) NOT NULL AUTO_INCREMENT ,
  `razon social` VARCHAR(45) NULL DEFAULT NULL ,
  `direccion` VARCHAR(45) NULL DEFAULT NULL ,
  `telefono` VARCHAR(15) NULL DEFAULT NULL ,
  `ruc` CHAR(11) NULL DEFAULT NULL ,
  `estado` BINARY(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`idproveedor`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaweb`.`producto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemaweb`.`producto` (
  `idproducto` INT(11) NOT NULL AUTO_INCREMENT ,
  `descripcion` VARCHAR(45) NULL DEFAULT NULL ,
  `precio` DOUBLE NULL DEFAULT NULL ,
  `stock` INT(11) NULL DEFAULT NULL ,
  `idproveedor` INT(11) NULL DEFAULT NULL ,
  `estado` BINARY(1) NULL DEFAULT NULL ,
  `estadoIGV` BINARY(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`idproducto`) ,
  INDEX `producto_proveedor` (`idproveedor` ASC) ,
  CONSTRAINT `producto_proveedor`
    FOREIGN KEY (`idproveedor` )
    REFERENCES `sistemaweb`.`proveedor` (`idproveedor` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaweb`.`detalle_ventas`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemaweb`.`detalle_ventas` (
  `iddetalle_ventas` INT(11) NOT NULL AUTO_INCREMENT ,
  `cantidad` INT(11) NULL DEFAULT NULL ,
  `subtotal` DOUBLE NULL DEFAULT NULL ,
  `subtotalconIGV` DOUBLE NULL DEFAULT NULL ,
  `idcomprobante` INT(11) NULL DEFAULT NULL ,
  `idproducto` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`iddetalle_ventas`) ,
  INDEX `detalleVenta_producto` (`idproducto` ASC) ,
  INDEX `detalleVenta_comprobante` (`idcomprobante` ASC) ,
  INDEX `detalleVenta_comprobante` (`idcomprobante` ASC) ,
  CONSTRAINT `detalleVenta_producto`
    FOREIGN KEY (`idproducto` )
    REFERENCES `sistemaweb`.`producto` (`idproducto` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `detalleVenta_comprobante`
    FOREIGN KEY (`idcomprobante` )
    REFERENCES `sistemaweb`.`comprobante` (`idcomprobante` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `sistemaweb`.`tipo_producto`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sistemaweb`.`tipo_producto` (
  `idtipo_producto` INT(11) NOT NULL AUTO_INCREMENT ,
  `descripcion` VARCHAR(45) NULL DEFAULT NULL ,
  `producto_idproducto` INT(11) NOT NULL ,
  PRIMARY KEY (`idtipo_producto`) ,
  INDEX `fk_tipo_producto_producto1` (`producto_idproducto` ASC) ,
  CONSTRAINT `fk_tipo_producto_producto1`
    FOREIGN KEY (`producto_idproducto` )
    REFERENCES `sistemaweb`.`producto` (`idproducto` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
