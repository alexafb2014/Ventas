package app.controlador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import app.entidad.Empleado;
import app.servicio.EmpleadoServicio;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author i5
 */
@WebServlet(urlPatterns = {"/ControladorEmpleado"})
public class ControladorEmpleado extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String operacion = request.getParameter("operacion");
        EmpleadoServicio servicio = new EmpleadoServicio();
        
        

        if (operacion.equals("ListarEmpleados")) {
            try {
                ArrayList<Empleado> empleados = servicio.listarEmpleados();
                request.setAttribute("empleados", empleados);

            } catch (Exception e) {
                request.setAttribute("error", e.getMessage());

            }
            RequestDispatcher rd = request.getRequestDispatcher("vistaListarEmpleados.jsp");
            rd.forward(request, response);
        }

        
        if (operacion.equals("RegistrarEmpleado")) {
            try {
                String nombre=request.getParameter("txtnombre");
                String apellido=request.getParameter("txtapellidos");
                String dni=request.getParameter("txtdni")+"";
                String direccion=request.getParameter("txtdireccion");
                String telefono=request.getParameter("txttelefono")+"";
                int estado=1;
                                
                Empleado empleado = new Empleado(nombre, apellido, dni, direccion, telefono, estado);
                request.setAttribute("empleado", empleado);
                servicio.RegistrarEmpleado(empleado);                
            } catch (Exception e) {
                request.setAttribute("error", e.getMessage());
            }
            RequestDispatcher rd = request.getRequestDispatcher("vistaRegistrarEmpleado.jsp");
            rd.forward(request, response);
        }
        if (operacion.equals("BuscarEmpleado")) {
            try {
                int id= Integer.parseInt(request.getParameter("txtid"));
                ArrayList<Empleado> empleado = servicio.BuscarEmpleado(id);
                request.setAttribute("empleados", empleado);

            } catch (Exception e) {
                request.setAttribute("error", e.getMessage());

            }
            RequestDispatcher rd = request.getRequestDispatcher("vistaListarEmpleados.jsp");
            rd.forward(request, response);
        }
        if (operacion.equals("EliminarEmpleado")) {
            try {
                
                int id= Integer.parseInt(request.getParameter("txtid"));
                Empleado empleado = new Empleado(id);
                
                    servicio.EliminarEmpleado(empleado);
                    request.setAttribute("empleado", empleado);
                
                
            } catch (Exception e) {
                request.setAttribute("error", e.getMessage());
            }
            RequestDispatcher rd = request.getRequestDispatcher("VistaEliminar.jsp");
            rd.forward(request, response);
        }
         if (operacion.equals("ModificarEmpleado")) {
            try {
                
                int id= Integer.parseInt(request.getParameter("txtid"));
                String nombre = request.getParameter("txtnombre");
                String apellido = request.getParameter("txtapellido");
                String dni = request.getParameter("txtdni")+"";
                String direccion = request.getParameter("txtdireccion");
                String telefono = request.getParameter("txttelefono")+"";
                Empleado empleado=new Empleado(id, nombre, apellido, dni, direccion, telefono,1);
                 servicio.ModificarEmpleado(empleado);
                  request.setAttribute("empleado", empleado);
                  
            } catch (Exception e) {
                request.setAttribute("error", e.getMessage());
            }
            RequestDispatcher rd = request.getRequestDispatcher("VistaModificar.jsp");
            rd.forward(request, response);
        }
         if (operacion.equals("FormRegistrar")) {
            response.sendRedirect("formRegistrarEmpleado.jsp");
        }
        if (operacion.equals("FormEliminar")) {
            response.sendRedirect("formEliminarEmpleado.jsp");
        }
        if (operacion.equals("FormBuscar")) {
            response.sendRedirect("formBuscarEmpleado.jsp");
        }
        if (operacion.equals("FormModificar")) {
            response.sendRedirect("formModificarEmpleado.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

