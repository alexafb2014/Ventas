/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controlador;
import app.entidad.Cliente;
import app.servicio.clienteServicio;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Dora
 */
@WebServlet(urlPatterns = {"/ControladorCliente"})
public class ControladorCliente extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String operacion = request.getParameter("operacion");
        clienteServicio servicio1 = new clienteServicio();

        if (operacion.equals("Listar")) {
            try {
                ArrayList<Cliente> clientes = servicio1.listarClientes();
                request.setAttribute("clientes", clientes);

            } catch (Exception e) {
                request.setAttribute("error", e.getMessage());

            }
            RequestDispatcher rd = request.getRequestDispatcher("VistaListaClientes.jsp");
            rd.forward(request, response);
        }

        
        if (operacion.equals("Registrar")) {
            try {
               
                String nombre=request.getParameter("txtnombre");
                String apellido=request.getParameter("txtapellido");
                String dni=request.getParameter("txtdni");
                String direccion=request.getParameter("txtdireccion");
                String telefono=request.getParameter("txttelefono");
                 String ruc=request.getParameter("txtruc");
              
                                
      
                Cliente cliente=new Cliente(nombre,apellido,dni,direccion,telefono,ruc);
                request.setAttribute("cliente", cliente);
                servicio1.RegistrarCliente(cliente);                
            } catch (Exception e) {
                request.setAttribute("error", e.getMessage());
            }
            RequestDispatcher rd = request.getRequestDispatcher("vistaRegistrarProducto.jsp");
            rd.forward(request, response);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}


