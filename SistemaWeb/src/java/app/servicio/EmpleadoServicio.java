/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.servicio;

import app.dao.EmpleadoDao;
import app.entidad.Empleado;
import java.util.ArrayList;

/**
 *
 * @author Moisés Saavedra
 */
public class EmpleadoServicio {
            
    public void RegistrarEmpleado(Empleado e) throws Exception{
        if(e==null) throw new Exception("El empleado está vacío");
        EmpleadoDao dao= new EmpleadoDao(e);
        dao.registrar();        
    }

    public ArrayList<Empleado> listarEmpleados() throws Exception {
          return new EmpleadoDao().listar();
    }
    
public ArrayList BuscarEmpleado(int id) throws Exception{
        return new EmpleadoDao().listarBusqueda(id);
    }

    public void EliminarEmpleado(Empleado empleado) throws Exception {
          if(empleado==null) throw new Exception("El Empleado está vacío");
          EmpleadoDao dao= new EmpleadoDao(empleado);
          dao.Eliminar();    
          
          
    }

    public void ModificarEmpleado(Empleado empleado) throws Exception {
        if(empleado==null) throw new Exception("El Empleado está vacío");
        EmpleadoDao dao= new EmpleadoDao(empleado);
        dao.Modificar();       
    }
 
}
