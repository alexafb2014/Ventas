/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.servicio;

import app.dao.DatosEmpresaDao;
import app.entidad.DatosEmpresa;
import java.util.ArrayList;

/**
 *
 * @author ANDERSSON
 */
public class DatosServicio {
    
    
    public ArrayList<DatosEmpresa> listar() throws Exception {
          return new DatosEmpresaDao().listar();
    }

    public void ModificarDatos(DatosEmpresa datos) throws Exception {
       if(datos==null) throw new Exception("Los datos están vacíos");
        DatosEmpresaDao dao= new DatosEmpresaDao(datos);
        dao.Modificar();  
    
    }
    
}
