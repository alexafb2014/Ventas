/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import app.entidad.DatosEmpresa;
import base.datos.BaseDatos;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author ANDERSSON
 */
public class DatosEmpresaDao {

    private DatosEmpresa datos;

    public DatosEmpresaDao(DatosEmpresa d) {
        
        this.datos = d;
    }

    public DatosEmpresaDao() {
    }
    
    
    public ArrayList<DatosEmpresa> listar() throws Exception {

        BaseDatos.conectar();
        String sql="SELECT * FROM Empleado where id=1";        
        ResultSet rs = BaseDatos.consultar(sql);
        ArrayList<DatosEmpresa> datosempresa = new ArrayList<DatosEmpresa>();
        while(rs.next()){
            DatosEmpresa temp= new DatosEmpresa(rs.getInt("id"),
                    rs.getString("razonsocial"),
                    rs.getString("ruc"),
                    rs.getString("telefono"),
                    rs.getString("direccion")
                    );
            datosempresa.add(temp);
        }
        BaseDatos.desconectar();
        return datosempresa; 
    }

    public void Modificar() throws Exception {
        BaseDatos.conectar();
        String sql="update datosempresa "
                +"set razonsocial='"+datos.getRazonsocial()+"'"
                +", ruc='"+datos.getRuc()+"'"
                +", direccion='"+datos.getDireccion()+"'"
                +", telefono='"+datos.getTelefono()+"'"
                +" where id="+datos.getCodigo();
        
        BaseDatos.ejecutar(sql);
        BaseDatos.desconectar();
    
    }
    
}
