/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.dao;

import app.entidad.Empleado;
import base.datos.BaseDatos;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author ANDERSSON
 */
public class EmpleadoDao {

    
    private Empleado empleado;

    public EmpleadoDao() {
    }
    
    
    public EmpleadoDao(Empleado e) {
       
        this.empleado=e;
    }

    public void registrar() throws Exception {

        BaseDatos.conectar();
        String sql="INSERT INTO empleado (nombre,apellido,dni,direccion,telefono,estado) ";
        sql+="VALUES('"+empleado.getNombre()+"','";
        sql+=empleado.getApellido()+"','"+empleado.getDni()+"','"+empleado.getDireccion()+"','"+
             empleado.getTelefono()+"',"+empleado.getEstado()+")";
        BaseDatos.ejecutar(sql);
        BaseDatos.desconectar();
    }
    public void Eliminar() throws Exception{
        BaseDatos.conectar();
        String sql="update empleado set estado="+0+" where idempleado="+empleado.getCodigo();
        
        BaseDatos.ejecutar(sql);
        BaseDatos.desconectar();
        
        
    }
    
    public void Modificar() throws Exception {
        BaseDatos.conectar();
        String sql="update empleado "
                +"set nombre='"+empleado.getNombre()+"'"
                +", apellido='"+empleado.getApellido()+"'"
                +", dni='"+empleado.getDni()+"'"
                +", direccion='"+empleado.getDireccion()+"'"
                +", telefono='"+empleado.getTelefono()+"'"
                +" where idempleado="+empleado.getCodigo();
        
        BaseDatos.ejecutar(sql);
        BaseDatos.desconectar();
    }
    
    
    
    public ArrayList listar() throws Exception {
        BaseDatos.conectar();
        String sql="SELECT * FROM empleado where estado= 1";        
        ResultSet rs = BaseDatos.consultar(sql);
        ArrayList<Empleado> empleados = new ArrayList<Empleado>();
        while(rs.next()){
            Empleado temp= new Empleado(rs.getInt("idempleado"),
                    rs.getString("nombre"),
                    rs.getString("apellido"),
                    rs.getString("dni"),
                    rs.getString("direccion"),
                    rs.getString("telefono"),
                    rs.getInt("estado"));
            empleados.add(temp);
        }
        BaseDatos.desconectar();
        return empleados;        
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public ArrayList listarBusqueda(int id) throws Exception{
        BaseDatos.conectar();
        String sql="SELECT * FROM empleado WHERE idempleado="+id+" and estado="+1;        
        ResultSet rs = BaseDatos.consultar(sql);
        ArrayList<Empleado> empleados = new ArrayList<Empleado>();
        while(rs.next()){
            Empleado temp= new Empleado(rs.getInt("idempleado"),
                    rs.getString("nombre"),
                    rs.getString("apellido"),
                    rs.getString("dni"),
                    rs.getString("direccion"),
                    rs.getString("telefono"),
                    rs.getInt("estado"));
            empleados.add(temp);
        }
        BaseDatos.desconectar();
        return empleados;   
    }
}
