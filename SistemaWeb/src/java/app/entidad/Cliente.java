/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.entidad;

/**
 *
 * @author Dora
 */
public class Cliente {
    private int idcliente;
    private String nombre;
    private String apellido;
    private String dni; 
   private String direccion;
     private String telefono;
    private String ruc;
    
    
    public Cliente(){
        
    }

    public Cliente(int idcliente, String nombre, String apellido, String dni, String direccion, String telefono, String ruc) {
        this.idcliente = idcliente;
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.direccion = direccion;
        this.telefono = telefono;
        this.ruc = ruc;
    }

    public Cliente(String nombre, String apellido, String dni, String direccion, String telefono, String ruc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getIdcliente() {
        return idcliente;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public String getDni() {
        return dni;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getRuc() {
        return ruc;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }
    
    
    
    
}

    

