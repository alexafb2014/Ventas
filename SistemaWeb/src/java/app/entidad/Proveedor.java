/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.entidad;

/**
 *
 * @author Dora
 */
public class Proveedor {
private int idproveedor;
    private String razonSocial;    
   private String direccion;
     private String telefono;
    private String ruc;
       private boolean estado;

    public Proveedor() {
    }

    public Proveedor(int idproveedor, String razonsocial, String direccion, String telefono, String ruc) {
        this.idproveedor = idproveedor;
        this.razonSocial = razonsocial;
        this.direccion = direccion;
        this.telefono = telefono;
        this.ruc = ruc;
        this.estado = true;
    }

    public Proveedor(String razonSocial, String direccion, String telefono, String ruc) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getIdproveedor() {
        return idproveedor;
    }

    public String getRazonsocial() {
        return razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getRuc() {
        return ruc;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setIdproveedor(int idproveedor) {
        this.idproveedor = idproveedor;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonSocial = razonsocial;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
}


    

