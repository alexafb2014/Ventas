/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.entidad;

/**
 *
 * @author ANDERSSON
 */
public class DatosEmpresa {
    int codigo;
    String razonsocial;
    String ruc;
    String direccion;
    String telefono;

    public DatosEmpresa(int codigo, String razonsocial, String ruc, String direccion, String telefono) {
        this.codigo = codigo;
        this.razonsocial = razonsocial;
        this.ruc = ruc;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public DatosEmpresa(String razonsocial, String ruc, String direccion, String telefono) {
        this.razonsocial = razonsocial;
        this.ruc = ruc;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    
    
}
