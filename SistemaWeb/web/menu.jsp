<%-- 
    Document   : menu
    Created on : 21/10/2014, 07:11:54 PM
    Author     : i5
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Operaciones de producto</h1>
        <ul>
            <li><a href="./ControladorProducto?operacion=FormRegistrar">Nuevo producto</a></li>
            <li><a href="./ControladorProducto?operacion=FormEliminar">Eliminar producto</a></li>
            <li><a href="./ControladorProducto?operacion=FormModificar">Modificar producto</a></li>
            <li><a href="./ControladorProducto?operacion=FormBuscar">Buscar producto</a></li>
            <li><a href="./ControladorProducto?operacion=Listar">Listar productos</a></li>
        </ul>
        <h1>Operaciones de Empleado</h1>
        <ul>
            <li><a href="./ControladorEmpleado?operacion=FormRegistrar">Nuevo Empleado</a></li>
            <li><a href="./ControladorEmpleado?operacion=FormEliminar">Eliminar Empleado</a></li>
            <li><a href="./ControladorEmpleado?operacion=FormModificar">Modificar Empleado</a></li>
            <li><a href="./ControladorEmpleado?operacion=FormBuscar">Buscar Empleado</a></li>
            <li><a href="./ControladorEmpleado?operacion=ListarEmpleados">Listar Empleados</a></li>
        </ul>
    </body>
</html>
