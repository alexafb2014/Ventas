<%-- 
    Document   : VistaEliminar
    Created on : 06-nov-2017, 21:29:53
    Author     : ANDERSSON
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="app.entidad.*"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
        <title>JSP Page</title>
    </head>
    <body>
        
        <h1>Eliminacion</h1>
        <%
            String error = (String) request.getAttribute("error");
            if (error != null) {
                out.println("No se pudo Eliminar debido "
                        + "al siguiente error:" + error);
            } else {
                
                out.println( "Eliminacion exitosa");
            }
        %>
        
<a href="menu.jsp">Regresar al menu principal</a>
    </body>
</html>
