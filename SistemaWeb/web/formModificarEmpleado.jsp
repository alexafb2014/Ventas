<%-- 
    Document   : formModificarEmpleado
    Created on : 30-nov-2017, 15:01:20
    Author     : ANDERSSON
--%>

<%@page import="app.entidad.Empleado"%>
<%@page import="java.util.ArrayList"%>
<%@page import="app.servicio.EmpleadoServicio"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Modificar producto</h1>
        
            <form action="ControladorEmpleado">
                <tr>
                    <td>Ingresar ID del Empleado a Modificar</td>
                    <td><input type="number" name="txtid" required/></td>
                </tr> <br>
                <tr>
                    <td>Ingresar nueva nombre</td>
                    <td><input type="text" name="txtnombre" required/></td>
                </tr><br>
                <tr>
                    <td>Ingresar nuevo apellido</td>
                    <td><input type="text" name="txtapellido" required/></td>
                </tr><br>
                <tr>
                    <td>Ingresar nuevo DNI</td>
                    <td><input type="number" name="txtdni" required/></td>
                </tr>
                 <tr>
                    <td>Ingresar nueva direccion</td>
                    <td><input type="text" name="txtdireccion" required/></td>
                </tr><br>
                 <tr>
                    <td>Ingresar nuevo Telefono</td>
                    <td><input type="number" name="txttelefono" required/></td>
                </tr><br>
                    <td><input type="submit" name="operacion" value="ModificarEmpleado"/></td>
                    
                    
                
            </form> <br>
            <br>
             <a href="menu.jsp">Regresar al menu principal</a>
            <table border="2">
                <tr>
                    <td>Codigo</td>
                    <td>Nombre</td>
                    <td>Apellidos</td>
                    <td>DNI</td>
                    <td>Direccion</td>
                    <td>Telefono</td>
                </tr>
            <%
                EmpleadoServicio servicio = new EmpleadoServicio();
                try {
                ArrayList<Empleado> empleados = servicio.listarEmpleados();
                request.setAttribute("empleados", empleados);

            } catch (Exception e) {
                request.setAttribute("error", e.getMessage());

            }
                ArrayList<Empleado> empleados = (ArrayList) request.getAttribute("empleados");
                if (empleados != null) {
                    for (Empleado p : empleados) {
                        out.println("<tr> "
                                + "<td>" + p.getCodigo()+ "</td>"
                                + "<td>" + p.getNombre()+ "</td>"
                                + "<td>" + p.getApellido()+ "</td>"
                                + "<td> " + p.getDni()+ " </td> "
                                + "<td> " + p.getDireccion()+ " </td> "
                                + "<td> " + p.getTelefono()+ " </td> "
                                + "</tr>");
                    }
                } else {
                    out.println("No hay datos de Empleados");
                }
                
            %>
        </table>

    </body>
</html>

