<%-- 
    Document   : vistaRegistrarEmpleado
    Created on : 30-nov-2017, 14:19:47
    Author     : ANDERSSON
--%>

<%@page import="app.entidad.Empleado"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Registro de Empleado</h1>
        <%
            String error = (String) request.getAttribute("error");
            if (error != null) {
                out.println("No se pudo registrar empleado debido "
                        + "al siguiente error:" + error);
            } else {
                Empleado p = (Empleado) request.getAttribute("empleado");
                out.println(p.getNombre()+" "+ p.getApellido()+ " registrado exitosamente");
            }
        %>
        
 <a href="menu.jsp">IRegresar al menu principal</a>
    </body>
</html>
