<%-- 
    Document   : formEliminarEmpleado
    Created on : 30-nov-2017, 14:40:17
    Author     : ANDERSSON
--%>

<%@page import="app.entidad.Empleado"%>
<%@page import="java.util.ArrayList"%>
<%@page import="app.servicio.EmpleadoServicio"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Eliminar producto</h1>
        
            <form action="ControladorEmpleado">
                <tr>
                    <td>Ingresar ID del Empleado a eliminar</td>
                    <td><input type="number" name="txtid" required/></td>
                </tr>
                
                    <td><input type="submit" name="operacion" value="EliminarEmpleado"/></td>
                    
                    
                
            </form> <br>
            <br>
             <a href="menu.jsp">Regresar al menu principal</a>
            <table border="2">
                <tr>
                     <th>Codigo</th>
                     <th>Nombre</th>
                     <th>Apellidos</th>
                     <th>DNI</th>
                     <th>Direccion</th>
                     <th>Telefono</th>
                </tr>
            <%
                EmpleadoServicio servicio = new EmpleadoServicio();
                try {
                ArrayList<Empleado> empleados = servicio.listarEmpleados();
                request.setAttribute("empleados", empleados);

            } catch (Exception e) {
                request.setAttribute("error", e.getMessage());

            }
                ArrayList<Empleado> empleados = (ArrayList) request.getAttribute("empleados");
                if (empleados != null) {
                    for (Empleado p : empleados) {
                        out.println("<tr> "
                                + "<td>" + p.getCodigo()+ "</td>"
                                + "<td>" + p.getNombre()+ "</td>"
                                + "<td>" + p.getApellido()+ "</td>"
                                + "<td> " + p.getDni()+ " </td> "
                                + "<td> " + p.getDireccion()+ " </td> "
                                + "<td> " + p.getTelefono()+ " </td> "
                                + "</tr>");
                    }
                } else {
                    out.println("No hay datos de Empleados");
                }
                
            %>
        </table>

    </body>
</html>
