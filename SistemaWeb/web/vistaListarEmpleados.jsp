

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.*,app.entidad.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
<a href="menu.jsp">Regresar al menu principal</a>
        <h1>Resultado De Busqueda</h1>
        <table border="2">
            <tr>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>DNI</th>
                <th>Direccion</th>
                <th>Telefono</th>
                
            </tr>
            <%
             
                ArrayList<Empleado> empleados = (ArrayList) request.getAttribute("empleados");
                if (empleados != null) {
                    for (Empleado p : empleados) {
                        out.println("<tr> <td>" + p.getNombre()+ "</td>"
                                + "<td>" + p.getApellido()+ "</td>"
                                + "<td> " + p.getDni()+ " </td> "
                                + "<td> " + p.getDireccion()+ " </td> "
                                + "<td> " + p.getTelefono()+ " </td> "
                                + "</tr>");
                    }
                } else {
                    out.println("No hay datos de Empleados");
                }
            %>
        </table>
    </body>
</html>
