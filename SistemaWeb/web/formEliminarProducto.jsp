<%-- 
    Document   : formEliminarProducto
    Created on : 06-nov-2017, 20:11:46
    Author     : ANDERSSON
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="app.entidad.Producto"%>
<%@page import="app.servicio.ProductoServicio"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Eliminar producto</h1>
        
            <form action="ControladorProducto">
                <tr>
                    <td>Ingresar ID del producto a eliminar</td>
                    <td><input type="text" name="txtid" required/></td>
                </tr>
                
                    <td><input type="submit" name="operacion" value="EliminarProducto"/></td>
                    
                    
                
            </form> <br>
            <br>
             <a href="menu.jsp">Regresar al menu principal</a>
            <table border="2">
                <tr>
                    <td>Codigo</td>
                    <td>Descripcion</td>
                    <td>Precio</td>
                    <td>Stock</td>
                </tr>
            <%
                ProductoServicio servicio = new ProductoServicio();
                try {
                ArrayList<Producto> productos = servicio.listarProductos();
                request.setAttribute("productos", productos);

            } catch (Exception e) {
                request.setAttribute("error", e.getMessage());

            }
                ArrayList<Producto> productos = (ArrayList) request.getAttribute("productos");
                if (productos != null) {
                    for (Producto p : productos) {
                        out.println("<tr> "
                                + "<td>" + p.getId() + "</td>"
                                + "<td>" + p.getDescripcion() + "</td>"
                                + "<td>" + p.getPrecio() + "</td>"
                                + "<td> " + p.getStock() + " </td> </tr>");
                    }
                } else {
                    out.println("No hay datos de productos");
                }
                
            %>
        </table>

    </body>
</html>
