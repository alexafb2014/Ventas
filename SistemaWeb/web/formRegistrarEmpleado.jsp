<%-- 
    Document   : RegistrarEmpleado
    Created on : 30-nov-2017, 14:02:33
    Author     : ANDERSSON
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Nuevo Empleado</h1>
        <table>
            <form action="ControladorEmpleado">
                <tr>
                    <td>Nombres</td>
                    <td><input type="text" name="txtnombre" required /></td>
                </tr>
                <tr>
                    <td>Apellidos</td>
                    <td><input type="text" name="txtapellidos" required /></td>
                </tr>
                <tr>
                    <td>DNI</td>
                    <td><input type="number" name="txtdni" required /></td>
                </tr>
                <tr>
                    <td>Direccion</td>
                    <td><input type="text" name="txtdireccion" required /></td>
                </tr>
                <tr>
                    <td>Telefono</td>
                    <td><input type="number" name="txttelefono" required /></td>
                </tr>
                <tr>
                    <td><input type="submit" name="operacion" value="RegistrarEmpleado"/></td>
                    
                </tr>
            </form>
        </table>
        <a href="menu.jsp">Regresar al menu principal</a>

    </body>
</html>