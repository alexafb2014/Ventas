<%-- 
    Document   : formListarProductos
    Created on : 21/10/2014, 07:11:11 PM
    Author     : i5
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.*,app.entidad.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
<a href="menu.jsp">IRegresar al menu principal</a>
        <h1>Resultado</h1>
        <table border="2">
            <tr>
                <th>Descripcion</th>
                <th>Precio</th>
                <th>Stock</th>
            </tr>
            <%
             
                ArrayList<Producto> productos = (ArrayList) request.getAttribute("productos");
                if (productos != null) {
                    for (Producto p : productos) {
                        out.println("<tr> <td>" + p.getDescripcion() + "</td>"
                                + "<td>" + p.getPrecio() + "</td>"
                                + "<td> " + p.getStock() + " </td> </tr>");
                    }
                } else {
                    out.println("No hay datos de productos");
                }
            %>
        </table>
    </body>
</html>
